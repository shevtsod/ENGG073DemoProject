# Class containing custom math methods, mirroring the
# default Ruby Math class
class MyMath
  # Sum an arbitrary amount of numbers
  def self.sum(*args)
    args.reduce(0) { |a, b| a + b }
  end
end

puts 'Running tests for Custom math class "MyMath"'
puts

# Test sum with different numbers
puts "5 + 5     = #{MyMath.sum(5, 5)}"
puts "6 + -12   = #{MyMath.sum(6, -12)}"
puts "0.5 + 0.3 = #{MyMath.sum(0.5, 0.3)}"

# Test case where only one number was passed in
puts "5 = #{MyMath.sum(5)}"

# Test case where no argument was passed in
puts "0 = #{MyMath.sum}"

# Test cases with multiple numbers
puts "2 - 3.3 + 5.4 = #{MyMath.sum(2, -3.3, 5.4).round(1)}"

puts
puts 'All tests completed successfully!'
